## The Mechanics of Modern Thievery

*An introduction to secrets hunting for beginners by Greg Johnson @ GitLab*

### Setting the scene

Imagine, for a moment, that you are a career criminal. A burglar, at that. You're very good at your job. It's a quiet morning in the business district of the area you've chosen to case. You've been watching a target for several days. It's a large box of a building; a plain, brass, nondescript, standard key is required for entry. Hundreds if not thousands of employees all coming and going during the morning hours.

You watch as an employee gets out of their car to go into work. They look tired, reluctant to enter the building where they will spend most of their day. As they close the door to their car, the key to get into the building slips from their very full hands and falls to the pavement. They don't notice. This is your moment.

You casually walk toward the dropped key as they begin walking toward the building. You pick up the key and, quickly, as to avoid notice, produce a small tin box filled with softened putty. You gently press the key into the putty and imprint its shape, snap the lid closed, and slip the box back into your pocket to later make a mold and copy the key.

"Excuse me?", you say, trying to contain your excitement. "You've dropped your key!"  You kindly hand the dropped key back to them, and walk towards your car across the street.

And just like that, you're in.

### The fundamentals of modern digital thievery

The intent of this short story is to draw a good metaphor from which to better explain the mechanics of modern digital thievery where we're no longer dealing with physical keys, but digital keys. Who among us has yet to lose a set of physical keys?  It happens quite often if I'm correct. We'll take a progressively more technical look at digital keys, how they get dropped in the digital world, and how to effectively pick them up before thieves do as we progress through the segments of the article, using our metaphor to better explain the basics.

### Expanding upon gitrob

In recent weeks, the Red Team at GitLab has been busy updating a tool called gitrob to aid our operations. The tool, originally written and open sourced by [Michael Henriksen](https://github.com/michenriksen), was a perfect stepping stone to create a sophisticated historical git repository secrets scanner. Until recently, open source tooling for scanning GitLab repositories for digital secrets on-demand have been few and far between. Originally, gitrob supported GitHub repositories only, and worked by looking for interesting files contained in the repository: files that often contain configuration information for common development platforms and hosting environments. Not only did we add support for GitLab repositories, but we built in additional functionality to support multiple modes by which you can scan for secrets improving your chances of finding a lost key. We also added the ability to optionally clone repositories into memory instead of to disk to aid search performance, as well as support for running gitrob within a docker container for better portability.

We’ll certainly go deeper into the details of each added feature later; however for the purpose of this first post in the series, it's only necessary that you have a basic understanding of [git](https://git-scm.com/), git repositories, and collaboration hosts like GitLab and what they're used for. This post will serve as a non-technical introduction, and a technical deep dive into the specifics of searching for digital tokens and automating that process for red teaming, penetration testing, and defense strategies will happen in later segments. 

### Keys: the move from physical to digital

Keys and locks have taken many forms over the centuries. Today, physical keys are the most commonly manufactured metal object, enabling us to live in the modern world where almost everything is behind a lock. They're tiny, easy to carry, and easy to use.

Keys and locks appeared in human history about 6,000 years ago. The first simple wooden ["pin tumbler" lock](https://en.wikipedia.org/wiki/Pin_tumbler_lock) used small pins hidden near the bolt along with a wooden key that raised the pins allowing the latch to open. Though the pin tumbler design is still in heavy use even today, there were some flaws in the original design, otherwise we'd still be using them. For example, the wooden components were very easy to open with brute force to name one.
 
![Egyption Lock](./images/egyption-lock.jpg)

Between then and now, we've employed different materials to make keys and locks, changed their shape, design, and even their size. We've even done away with physical keys completely, resulting in a heavy dependence on multiple forms of digital keys.

### The shape of modern keys

Today, keys are needed for unlocking more than just your car and your house. Different forms of digital keys are necessary en masse to access your computer, your bank account, your Facebook page, your work email, your personal email(s), and the list goes on. Software developers, systems administrators, and others responsible for building and maintaining the systems you need access to also need keys for similar, but slightly different needs: to login to the systems and networks that support the operation of the systems you rely on for services you use every single day.

More often than not, these keys take forms other than the traditional username and password combination we're all accustomed to, though these get leaked as often as any other type of digital key. Let's take a closer look at an example of what a modern digital key may look like:

```json
{
    "Secret": "BGrtmN19bR1097135vhjkKGgBM54912yuIPOn456"
}
```

A key like this might provide an administrator [authentication and authorization](https://www.okta.com/identity-101/authentication-vs-authorization/) into one or more systems that control how an application operates in order to fix bugs, adjust configuration, or add resources to adapt to demand. It might allow access to sensitive information needed to maintain those systems:  user accounts, passwords, proprietary data, and servers just for a start. It may have access to control DNS and domain name registrations which control an organization's presence on the internet. It might allow a user to start, stop, and create servers, or any billable resource from a cloud provider depending on its configuration.

Should a key like this fall into the wrong hands, it has the propensity to be extremely detrimental to business in terms of data exposure, resource costs, and time to remediate which could otherwise be spent moving the business forward. Just like a physical key, it's very difficult to tell precisely what doors it will open without trying a few of them to see for yourself. Should you come across a leaked key, it will often involve some ingenuity coupled with loads of caution to determine what precisely it has access to.

There are many types of files containing these keys that might be present on a developers' or administrators' machine, and they're often associated and stored with application software, code which makes the application work, or other similar assets. These configuration files are often shared with teams in collaboration host repositories like GitLab, some of which are public for the world to see, as is often the case with [GitLab's own repositories](https://about.gitlab.com/handbook/values/#transparency). Referring back to our metaphor, perhaps it's easier to think about these repositories that GitLab and others host as if it were the parking lot where our poor employee dropped their physical key because, similarly, these repositories are often where a key like the one we looked at above get dropped and, subsequently, picked up by a thief.

### Where do modern keys come from?

In short, and hopefully without getting too technical, modern keys are created by software that generates some random text that has cryptographic significance. Regardless of whether or not they are generated and issued by Google, GitLab, GitHub, or others, they typically take similar but slightly different textual form. Let's look at a few more examples of these types of keys. 

Because these examples sometimes include large sets of text, I've stored them in [GitLab snippets](https://docs.gitlab.com/ee/api/project_snippets.html) so that you can delve into each to get a better idea of how they look. Later in the series, this will be more important as we dive into the technical details of how to detect them in git repositories and [other places](https://about.gitlab.com/blog/2019/12/20/introducing-token-hunter/). For now, suffice it to say that each of these examples are in many ways similar but also noticeably different:

* [SSH key example](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/snippets/1976051)
* [GitLab personal access key Example](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/snippets/1976052)
* [Twitter access key example](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/snippets/1976053)
* [PGP key example](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/snippets/1976055)

### How do modern keys get dropped?

Modern keys get dropped as a result of human error. Referring back to our metaphor, we just have too many things in our proverbial hands and mistakes inevitably are made. As a [red teamer](https://about.gitlab.com/handbook/engineering/security/red-team/) at GitLab, I often build, modify and automate software tooling to aid our operations, using GitLab to store the source code and make sure it compiles and deploys properly. Recently, I modified [gitrob](https://github.com/codeEmitter/gitrob) for this very purpose. I too keep these types of digital keys on my machine. Not surprisingly, I too make mistakes.

As I automated the process that made gitrob work to aid an operation, I committed a change to an internal git repository and accidentally left a sensitive token in a one-off script during that commit. Luckily, this key was leaked on an internal GitLab repository, it was associated with a counterfeit account that I had created during the operation, and it had read-only access. I'd taken the time to scope the token properly. In other words, when I asked GitLab to generate the key for me, I had specified the [principle of least privilege](https://en.wikipedia.org/wiki/Principle_of_least_privilege). In this case, all I needed was read access. Simply put, I didn't need to change any information on GitLab with the key, so I didn't give the key that level of access.

Regardless, this is how keys get "dropped" in the modern world. In the above scenario, I'm the employee in the parking lot that had dropped their key. However, I got lucky, and it was my teammates that discovered the mistake and not a legitimate attacker. It was easy to remediate but it served as a stark reminder to me and my teammates how easily it can happen. It's ironic, and funny, and the impact in this case was minimal, but that's not always the case. A simple commit to a repository had exposed my key to a theoretical internal threat. The stakes are often higher than we like to think with other types of tokens that may have access to more sensitive information.

There are other avenues of leakage that happen through exposed application log files that record the happenings within a system, improperly implemented security features within the application, or vulnerable implementations of the key generation process itself. Despite what you may assume, [computers aren't great at math](https://blog.codinghorror.com/why-do-computers-suck-at-math/). Generating random sets of numbers and letters involves math and sometimes it's possible to directly guess what a computer might consider random.

## The impact of leaked secrets

As easy as it is for engineers to make a similar mistake as I did and leak sensitive information to a *public* git repository hosted on GitLab, GitHub, Bitbucket, and the like, the results can be absolutely devastating. A leaked key, one made public unintentionally, has the very real risk of impacting a business' ability to ***stay*** in business if they're unable to spend the time and effort to remediate it properly after a breach. 

A compromise caused by a leaked key can cause customers to leave due to loss of trust, deadlines missed due to the inordinate amount of time spent determining the breadth and depth of exposure that could otherwise be spent elsewhere.  The bottom line is that a compromise sourced from a token leak is expensive and human error is one of the most common vectors by which these types of modern keys get leaked.

### A glimpse at the breadth of the problem

Git repositories are only one vector exposing API tokens and cryptographic keys to theft. For now we'll stick very close to that paradigm so that we can better understand it, however, it's worth mentioning that these tokens can leak from places you may not have thought of yet, or that you may assume are obscure enough to be secure. Web-based tools like pastebin.com, ghostbin.co and the like are often used by developers and engineers to easily share short snippets of code, application configuration information, application logs, and, you guessed it, usernames, passwords, cryptographic keys, API tokens, and the list goes on.  One thing you can be sure of is that if you publish this kind of information publicly, someone will most likely be looking for it, and it may be [exposed in the most unexpected of places](https://twitter.com/leak_scavenger).  We’ll dive more deeply into the specifics of these other token leak vectors later on in this series of posts.

It's also important to know that sensitive information will sit in public places like git repositories exposed to the internet for, in some cases, years before being detected, if they are ever detected. Why?  Because detection, especially in large, active git repositories with lots of historical information is a difficult technical problem, not to mention some of the other vectors of exposure we briefly touched on.  Despite the creation of [effective tools](https://github.com/awslabs/git-secrets) to prevent leaks before they happen, there hasn't been enough effort from the information security community to inform consumers on the fundamentals of these types of modern keys: what they look like, how they come into existence, how they commonly get leaked, how to detect them in the wild, and what can be done to remediate a leaked key when, not if, it happens.

### History Repeats Itself

Git repositories are only one vector exposing API tokens and cryptographic keys to theft.  For this article we'll stick close to that paradigm so that we can better understand it.  However, in recent years, [Facebook reset 50 million account passwords](https://arstechnica.com/information-technology/2018/09/50-million-facebook-accounts-breached-by-an-access-token-harvesting-attack/) due to a API token harvesting attack that leaked these same types of modern keys.  GateHub, a popular cryptocurrency wallet website, [had a similar breach](https://gatehub.net/blog/gatehub-preliminary-statement/) that allowed attackers to steal cryptocurrency from users' online wallets.  Amazon Web Services [accidentally leaked a key](https://gizmodo.com/amazon-engineer-leaked-private-encryption-keys-outside-1841160934) very similar to the example above, though it's unclear what the impact could have been due to it being detected within 30 minutes of the leak and reported to AWS two hours later.  

This is best case scenario.  Sometimes these keys will sit in public repositories exposed to the Internet for years before being detected, if they ever are detected.  Why?  Because detection is a hard problem.  Despite many efforts to improve detection capabilities and implement preventive measures, there hasn't been enough effort from the information security community to inform the consumers of these types of modern keys on the basics:  what they look like, how they come into existence, how they commonly get leaked, how to detect them in the wild, and what can be done to remediate a leaked key when, not if, it happens.

### Up Next

In part one of this series we’ve established a foundation and rendered a metaphor that we will reuse throughout the series to better exemplify the mechanics of modern thievery. We looked at how modern keys often get exposed, and described briefly some of the work GitLab's Red Team has been doing to support its operations with gitrob. If you were looking for technical specifics on detection, automation, and performance, you're probably sorely disappointed, but fear not!  In part two of this series we'll dive deeply into some of the new features of gitrob and the concepts behind popular techniques used to detect modern digital keys.
