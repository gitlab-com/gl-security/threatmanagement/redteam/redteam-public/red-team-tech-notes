# Black Hat EU 2021 - Picking Lockfiles: Attacking and Defending Your Supply Chain

#### By [Dennis Appelt](https://gitlab.com/dappelt) and [Greg Johnson](https://gitlab.com/codeEmitter)

## Abstract

An advantage of open source software (OSS) development is that it enables contributions from the public, adding new features and improvements. This also makes OSS projects a target of supply chain attacks.  In our talk at Black Hat EU 2021, Dennis Appelt and I (Greg Johnson) present both an offensive and defensive perspective of a supply chain attack technique.  This technique hides malicious code in open source contributions and reduces the likelihood of the modifications being caught during a code review.

Lockfiles are used by modern package managers to allow deterministic resolution of dependencies necessary to run an application.  Our technique abuses lockfiles based on the observations that 1) package managers do not sufficiently verify the integrity of lockfiles, 2) lockfiles are machine-generated and small modifications are easily missed during code review due to the mass of changes included, and 3) there is prevalent use of third-party packages and package managers in open source software projects.

This talk has benefits for both red and blue teams.  For red teams, we demonstrate both manual and automated approaches for choosing targets and tampering lockfiles and discuss MITRE ATT&CK TTPs, making it easier to simulate this type of supply chain attack for security teams.  For blue teams, we offer advise on what to look for during code reviews, and provide a light-weight tool that verifies the integrity of a lockfile well suited for inclusion in CI pipelines.

## Black Hat EU 2021 Video

[![Picking Lockfiles: Attacking and Defending Your Supply Chain](https://img.youtube.com/vi/UkWJeqb8BZk/0.jpg)](https://www.youtube.com/watch?v=UkWJeqb8BZk)

[Download the Slides](./picking-lockfiles-slides.pdf)

## DARKReading Interview

In September, prior to the Black Hat EU event, we were invited by [DARKReading](https://www.darkreading.com/) to do a brief interview covering our talk at a high level.  You can hear candid details about our research and the broader risk supply chain attacks pose to organizations.

[![DARKReading - Defending the Open Source Supply Chain](./darkreading.png)](https://player.vimeo.com/video/639149377?h=af536f0ac5)

## Tooling Demos and Links

### Defensivive Tooling

[Untamper-my-lockfile](https://gitlab.com/gitlab-org/frontend/untamper-my-lockfile/) was built based on the premise that  the integrity of lockfiles is not sufficiently verified by package managers.  Defenders have to carefully review lockfile changes to prevent lockfile tampering.  Untamper-my-lockfile can be used to automate the verification process in CI pipelines or manually during code reviews.

[![asciicast](https://asciinema.org/a/h4RVCYDQfKRaUvaZUMFtzh3qo.svg)](https://asciinema.org/a/h4RVCYDQfKRaUvaZUMFtzh3qo)

### Offensive Tooling

Attackers can use lockfile tampering to introduce attacker-controlled dependencies into target projects.  [Bump-Key](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/bump-key) is an offensive tool for finding potential target dependencies within a project and automating the tampering process to inject attacker-controlled dependencies and reduce the likelihood of detection.

[![asciicast](https://asciinema.org/a/467970.svg)](https://asciinema.org/a/467970)

## Closing Remarks

A sincere and special thanks to Dennis Appelt for his collaborative support and excellent defensive tooling.  Thank you also to [Fahmida Rashid](https://www.darkreading.com/author/fahmida-y-rashid) from DARKReading for their patience and casual approach during our interview.  Thank you to [Liran Tal](https://www.linkedin.com/in/talliran) for their initial work on this subject.  We expand on Liran's work by presenting more methods for tampering lockfiles, applying it to additional programming ecosystems (Ruby on Rails and Go in addition to Node.js), and providing tools that verify the integrity of a lockfile as well as automate the tasks of targeting suitable dependencies and tampering a lockfile.  Thanks to [Steve Manzuik](https://gitlab.com/smanzuik) and the [GitLab Red Team](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam), [Frederic Loudet](https://gitlab.com/floudet) and [Chris Moberly](https://gitlab.com/cmoberly), for their participation, support, and encouragement and making it possible to share this information at Black Hat EU.
