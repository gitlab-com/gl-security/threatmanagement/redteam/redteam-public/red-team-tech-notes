# Red Team Tech Notes

This project has been moved to a new location. Please drop by and visit us there.

Thanks!

Links:
- [New static website](https://gitlab-com.gitlab.io/gl-security/security-tech-notes/red-team-tech-notes/)
- [New source project](https://gitlab.com/gitlab-com/gl-security/security-tech-notes)